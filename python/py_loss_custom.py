# -*- coding: utf-8 -*-
"""
Created on Tue Apr 21 11:54:19 2015

@author: gus
"""
caffe_root = '/home/gus/caffe/'  # this file is expected to be in {caffe_root}/examples
import sys
sys.path.insert(0, caffe_root + 'python')

import caffe
import numpy as np
 
class EucLossLayer(caffe.Layer):
 
    def setup(self, bottom, top):
        # check input pair
        if len(bottom) != 2:
            raise Exception("Need two inputs to compute distance.")
 
    def reshape(self, bottom, top):
        # check input dimensions match
        if bottom[0].count != bottom[1].count:
            raise Exception("Inputs must have the same dimension.")
        # difference is shape of inputs
        self.diff = np.zeros_like(bottom[0].data, dtype=np.float32)
        # loss output is scalar
        top[0].reshape(1)
 
    def forward(self, bottom, top):
        #The difference between the expected output and the actual output
        #I'm assuming here in this code that bottom[1] is the image data
        self.diff[...] = bottom[0].data - bottom[1].data
        
        top[0].data[...] = np.sum(self.diff**2) / 2.0
 
    def backward(self, top, propagate_down, bottom):
        #This backpropagates the (delta loss) / (delta sigma), where sigma
        #is the output of the activation function. 
        bottom[1].diff[...] = -1 * self.diff

