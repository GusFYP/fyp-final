# -*- coding: utf-8 -*-
"""
Created on Wed May 27 11:23:52 2015

@author: gus
"""

caffe_root = '/home/gus/caffe/'  # this file is expected to be in {caffe_root}/examples
import sys
sys.path.insert(0, caffe_root + 'python')

import caffe
import numpy as np


def linear(matrix_in):
    #Linear sum of pixels in the sheet (for a given x y coordinate)
    dims = matrix_in.shape
    matrix_lin = np.zeros_like(matrix_in[0, :, :], dtype=np.float32)
    for sheet in range(0, dims[0]):
        #Sum each dimention for all pixels x,y:
        matrix_lin = np.add(matrix_lin, matrix_in[sheet, :, :])
    
    return matrix_lin
    
class PoolLayer(caffe.Layer):
    
    def setup(self, bottom, top):
        # Set up diff and sig
        self.diff = np.zeros_like(bottom[0].data[0, 0, :, :], dtype=np.float32)
                
    def reshape(self, bottom, top):
        #HARDCODED SIZE
        top[0].reshape(1, 1, 512,512)
 
    def forward(self, bottom, top):
        
        #Create a numpy matrix of the input:
        input_matrix = np.array(bottom[0].data[0, :, :, :], dtype=np.float32)
        #Find sigmoid of input:
        self.diff[...] = linear(input_matrix)    
        #Pass this up
        top[0].data[0, 0, :, :] = self.diff
        
    def backward(self, top, propagate_down, bottom):
        #Backpropagate
        bottom[0].diff[...] = 1
