# -*- coding: utf-8 -*-
"""
Created on Wed May 27 11:23:52 2015

@author: gus
"""

caffe_root = '/home/gus/caffe/'  # this file is expected to be in {caffe_root}/examples
import sys
sys.path.insert(0, caffe_root + 'python')

import caffe
import numpy as np

class NormLayer(caffe.Layer):
    
    def setup(self, bottom, top):
        #Get dimentions of bottom blob:
        self.botshape = np.shape(bottom[0].data)
        print(self.botshape)
        #Preallocate top blob matrix:
        self.diff = np.zeros((self.botshape[0], self.botshape[1], self.botshape[2], self.botshape[3]))
        
                
    def reshape(self, bottom, top):
        #SEMI-HARDCODED SIZE
        print(self.botshape[1])
        top[0].reshape(self.botshape[0], self.botshape[1], self.botshape[2], self.botshape[3])
 
    def forward(self, bottom, top):
        
        #Create a numpy matrix of the input:
        input_matrix = np.array(bottom[0].data[:, :, :, :])    
        
        self.diff[...] = input_matrix
        
        #Pass this up
        top[0].data[:, :, :, :] = self.diff

