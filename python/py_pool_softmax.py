# -*- coding: utf-8 -*-
"""
Created on Wed May 27 11:23:52 2015

@author: gus
"""

caffe_root = '/home/gus/caffe/'  # this file is expected to be in {caffe_root}/examples
import sys
sys.path.insert(0, caffe_root + 'python')

import caffe
import numpy as np


def softmax(matrix_in):
    #Sigmoid sum of pixels in the sheet (for a given x y coordinate)
    dims = matrix_in.shape
    matrix_exp_sum = np.zeros_like(matrix_in[:, 0, :, :], dtype=np.float32)
    matrix_softmax = np.zeros_like(matrix_in, dtype=np.float32)
    
    #Subtract the maximum pixel from all pixels:
    #(does not change output but prevents overflow):
    matrix_in = matrix_in - matrix_in.max()
    
    for sheet in range(0, dims[1]):
        #Sum the exponentials of all input matrices
        matrix_exp_sum = np.add(matrix_exp_sum, np.exp(matrix_in[:, sheet, :, :]))
        
    #Softmax:
    for sheet in range(0, dims[1]):
        matrix_softmax[:, sheet, :, :] = np.divide(np.exp(matrix_in[:, sheet, :, :]), matrix_exp_sum)
    
    return matrix_softmax
    
class SMPoolLayer(caffe.Layer):
    
    def setup(self, bottom, top):
        #Get dimentions of bottom array:
        self.botshape = np.shape(bottom[0].data)
        #Prealloacte diff array:
        self.diff = np.zeros_like(bottom[0].data, dtype=np.float32)
        
        
                
    def reshape(self, bottom, top):
        top[0].reshape(self.botshape[0], self.botshape[1], self.botshape[2],self.botshape[3])
 
    def forward(self, bottom, top):
        #Create a numpy matrix of the input:
        input_matrix = np.array(bottom[0].data, dtype=np.float32)
        #Find sigmoid of input:
        self.diff[...] = softmax(input_matrix)
        #Pass this up
        top[0].data[...] = self.diff
        
    def backward(self, top, propagate_down, bottom):
        ##THIS MAY NEED WORK
        #Backpropagate
        bottom_array = self.diff * (1 - self.diff)
                        
        bottom[0].diff[...] = bottom_array
        


