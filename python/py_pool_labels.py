# -*- coding: utf-8 -*-
"""
Created on Wed May 27 11:23:52 2015

@author: gus
"""

caffe_root = '/home/gus/caffe/'  # this file is expected to be in {caffe_root}/examples
import sys
sys.path.insert(0, caffe_root + 'python')

import caffe
import numpy as np

    
class PoolLayer(caffe.Layer):
    
    def setup(self, bottom, top):
        self.bot = []
        for layer in range(len(bottom)):
            self.bot.append(bottom[layer].data[0, 0, :, :])
        self.inshape = np.shape(self.bot)
                
    def reshape(self, bottom, top):
        top[0].reshape(1, self.inshape[0], self.inshape[1], self.inshape[2])
 
    def forward(self, bottom, top):
        
        top[0].data[...] = self.bot
        
    def backward(self, top, propagate_down, bottom):
        #Backpropagate                  
        bottom.diff[...] = np.ones(self.inshape)


