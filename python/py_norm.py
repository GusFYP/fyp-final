# -*- coding: utf-8 -*-
"""
Created on Wed May 27 11:23:52 2015

@author: gus
"""

caffe_root = '/home/gus/caffe/'  # this file is expected to be in {caffe_root}/examples
import sys
sys.path.insert(0, caffe_root + 'python')

import caffe
import numpy as np

class NormLayer(caffe.Layer):
    
    def setup(self, bottom, top):
        # difference is shape of inputs
        self.diff = np.zeros_like(bottom[0].data, dtype=np.float32)
                
    def reshape(self, bottom, top):
        #HARDCODED SIZE
        top[0].reshape(1, 1, 512,512)
 
    def forward(self, bottom, top):
        
        #Create a numpy matrix of the input:
        input_matrix = np.array(bottom[0].data[0, :, :, :])    
        
        #Normalise and forward propagate:
        self.diff[...] = input_matrix / 510 + 0.5
        
        #Pass this up
        top[0].data[0, 0, :, :] = self.diff

