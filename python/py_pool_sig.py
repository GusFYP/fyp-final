# -*- coding: utf-8 -*-
"""
Created on Wed May 27 11:23:52 2015

@author: gus
"""

caffe_root = '/home/gus/caffe/'  # this file is expected to be in {caffe_root}/examples
import sys
sys.path.insert(0, caffe_root + 'python')

import caffe
import numpy as np


def sigmoid(matrix_in):
    #Sigmoid sum of pixels in the sheet (for a given x y coordinate)
    dims = matrix_in.shape
    matrix_sm = np.zeros_like(matrix_in[0, :, :], dtype=np.float32)
    for sheet in range(0, dims[0]):
        #Sum each dimention for all pixels x,y:
        matrix_sm = np.add(matrix_sm, matrix_in[sheet, :, :])
        
    #Sigmoid
    #print(matrix_sm[100][100])
    matrix_sm = 1 / (1 + np.exp(-1 * matrix_sm))
    
    return matrix_sm
    
class PoolLayer(caffe.Layer):
    
    def setup(self, bottom, top):
        # Set up diff and sig
        self.diff = np.zeros_like(bottom[0].data[0, 0, :, :], dtype=np.float32)
                
    def reshape(self, bottom, top):
        #HARDCODED SIZE
        top[0].reshape(1, 1, 512,512)
 
    def forward(self, bottom, top):
        
        #Create a numpy matrix of the input:
        input_matrix = np.array(bottom[0].data[0, :, :, :], dtype=np.float32)
        #Find sigmoid of input:
        self.diff[...] = sigmoid(input_matrix)
        #Pass this up
        top[0].data[0, 0, :, :] = self.diff
        
    def backward(self, top, propagate_down, bottom):
        #Backpropagate
        bottom_array = np.zeros_like(bottom[0].data)
        #bottom_array = self.diff * (1 - self.diff)]
        b_shape = np.shape(bottom_array)
        for layer in range(b_shape[1]):
            bottom_array[0, layer, :, :] = self.diff * (1 - self.diff)
                        
        bottom[0].diff[...] = bottom_array


