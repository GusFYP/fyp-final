# -*- coding: utf-8 -*-
"""
Created on Wed May 27 11:23:52 2015

@author: gus
"""

caffe_root = '/home/gus/caffe/'  # this file is expected to be in {caffe_root}/examples
import sys
sys.path.insert(0, caffe_root + 'python')

import caffe
import numpy as np
import matplotlib.pyplot as plt


def softmax(matrix_in):
    #Sigmoid sum of pixels in the sheet (for a given x y coordinate)
    dims = matrix_in.shape
    matrix_exp_sum = np.zeros_like(matrix_in[:, 0, :, :], dtype=np.float64)
    matrix_softmax = np.zeros_like(matrix_in, dtype=np.float64)
    
    #Subtract the maximum pixel from all pixels:
    #(does not change output but prevents overflow):
    matrix_sheet = matrix_in - matrix_in.max(1)
    
    for sheet in range(0, dims[1]):    
        #Sum the exponentials of all input matrices
        matrix_exp_sum = np.add(matrix_exp_sum, np.exp(matrix_sheet[0, sheet, :, :]))
        
    #Softmax:
    for sheet in range(0, dims[1]):
        matrix_softmax[:, sheet, :, :] = np.divide(np.exp(matrix_sheet[:, sheet, :, :]), matrix_exp_sum)
    
    return matrix_softmax
    
def make_label(label_in, shape):
    #Preallocate a matrix to store the labels
    label_out = np.zeros(shape)
    #For each sheet:
    for sheet in range(shape[1]):
        label_out[0, sheet, :, :] = (label_in == sheet)         
    
    return label_out
    
    
class SMPoolLossLayer(caffe.Layer):
    
    def setup(self, bottom, top):
        #Get dimentions of bottom array:
        self.botshape = np.shape(bottom[0].data)
        #Prealloacte diff array:
        self.diff = np.zeros_like(bottom[0].data, dtype=np.float128)
                
    def reshape(self, bottom, top):
        top[0].reshape(1)
        top[1].reshape(self.botshape[0], self.botshape[1], self.botshape[2], self.botshape[3])
 
    def forward(self, bottom, top):
        #Create a numpy matrix of the inputs:
        input_matrix = np.array(bottom[0].data, dtype=np.float128)
        input_label = np.array(bottom[1].data, dtype=np.float128)
        
        #Create *class* dimentional input label:
        self.input_label_MC = make_label(input_label, self.botshape)
        
        #Find sigmoid of input:
        sm_input = softmax(input_matrix)
        
        #Clip this so that we never get log(0) or log(1):
        self.diff[...] = np.clip(sm_input,1e-15, 1-1e-15)
        
        #Pass this up (CE loss)
        top[0].data[...] = -1 * np.sum(self.input_label_MC * np.log(self.diff))
        top[1].data[...] = self.diff
        
    def backward(self, top, propagate_down, bottom):
        ##THIS MAY NEED WORK
        #Backpropagate
        bottom_array = self.diff - self.input_label_MC
                        
        bottom[0].diff[...] = bottom_array
        


