# -*- coding: utf-8 -*-
"""
Created on Mon Sep 14 20:24:53 2015

@author: gus
"""

caffe_root = '/home/gus/caffe/'  # this file is expected to be in {caffe_root}/examples
import sys
sys.path.insert(0, caffe_root + 'python')

import caffe
import numpy as np

class PyAccuracy(caffe.Layer):
    
    def setup(self, bottom, top):
        # difference is shape of inputs
        pass
                
    def reshape(self, bottom, top):
        #HARDCODED SIZE
        top[0].reshape(1)
 
    def forward(self, bottom, top):
        
        #Calculate number of correct pixels:
        Correct = bottom[0] == bottom[1]
        Correct = np.array(Correct)
        num_correct = Correct.sum()
        #Calculate number of total pixels:
        num_total = Correct.size
        
        top[0].data[...] = num_correct / num_total
        