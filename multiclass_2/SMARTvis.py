# -*- coding: utf-8 -*-
"""
Created on Mon May 18 16:23:47 2015

@author: gus

"""
#Setting up and Module Imports
import sys
caffe_root = '/home/gus/caffe/' 
sys.path.insert(0, caffe_root + 'python')
import caffe
import shutil
import make_directory
import matplotlib.pyplot as plt
import numpy as np
import math
import os

import sys
sys.path.insert(0, '/home/gus/repos/multiclass_2/scripts')
from make_matrix import make_matrix

###############################################################################
#SPECIFIY DISPLAY METAPARAMETERS HERE:
iter_start = 15000
no_iter = 1
iter_step = 1
num_tests = 10
SAVE = 1
##
print_blobs = 0
##
print_output_per_iter = 0
out_name = 'output'

##
print_weights = 0
save_weights = 1
##
print_weights_iter = 0
##
no_outputs = 5
##
scale_op = 1

folder_path = '/home/gus/repos/multiclass_2'
solver_name = '/network_params.prototxt'
layer_name = '/layers.prototxt'

###############################################################################
#EXTRACT NN METAPARAMETERS HERE:
if SAVE:
    #From SOLVER:
    solver = open(folder_path + solver_name, 'r')
    for line in solver:
        #Find where LR is specified:
        if 'base_lr:' in line:
            learning_rate = line[9:].strip()
        #Find where momentum is specified:
        if 'momentum' in line:
            momentum = line[11:].strip()
    
    #From LAYER:
    layer = open(folder_path + layer_name, 'r')
    #For storing n0. conv layers
    CL_no = 0
    #For storing the number of nodes per hidden layer:
    N_no = []
    #For storing kernel size:
    K_size = []
    for line in layer:
        #Count the number of convolution layers:
        if 'convolution_layer' in line:
            CL_no = CL_no + 1
        if 'num_output' in line:
            N_no.append(line[16:].strip())
        if "#pool_type:" in line:
            pool_type = line[12:].strip()
        if "LossLayer" in line:
            loss_type = line[12:].strip()
        if "kernel_size" in line:
            K_size.append(line[17:].strip())
            
            
    
    meta_dir = 'smart_results' + \
        '/NN_STRUCT_' + str(N_no) + \
        '/LNR_RATE_' + str(learning_rate) + \
        '/' + str(pool_type.replace("'", "")) + \
        '/' + str(loss_type.replace("'", ""))

###############################################################################
#Create the new directory path:
if SAVE:
    test_dir = make_directory.make_directory(folder_path, meta_dir)
    print(test_dir)

#save the parameter records:
    shutil.copyfile(folder_path + solver_name, test_dir + '/SOLVER.txt')
    shutil.copyfile(folder_path + '/layers.prototxt', test_dir + '/LAYERS.txt')

###############################################################################
#Extract and print the layer values:

#A blank list to store the loss value (to verify that it's going down)
loss_record = []

test_model = 1

#Loop through the trained model's states
for iteration in range(iter_start, (iter_start + no_iter), iter_step):
    
    #Specify trained model and NN structure:
    MODEL_FILE = folder_path + layer_name
    PRETRAINED = folder_path + '/snapshots/_iter_' + str(iteration) + '.caffemodel'
    net = caffe.Net(MODEL_FILE, PRETRAINED,caffe.TEST)
    
    if SAVE:
        #Save the pretrained model proto:
        shutil.copyfile(PRETRAINED, test_dir + '/_iter_' + str(iteration) + '.caffemodel')
       
    ##DISPLAY THE WEIGHTS:
    if print_weights_iter: 
        params = net.params.keys()
        weights = []
        for net_layer in params:
            weight_layer = net.params[net_layer][0].data
            weights.append(weight_layer)
            for weight_slice in range(len(weight_layer)):
                fig = plt.figure()
                plt.imshow(weight_layer[weight_slice][0], interpolation='none')
                fig.suptitle('WEIGHT_' + net_layer + '_' + str(weight_slice))    
    
    
    for test_image in range(num_tests):
        #Forward propagate:
        net.forward() 
    
        
        #The record of the loss over the iterations
        loss = net.blobs['loss'].data
        loss_record.append(loss)
        
        if SAVE:
            #Save loss record:
            loss_file = open(test_dir + '/LOSS.txt', 'a')
            loss_file.write("iter:")
            loss_file.write(str(iteration))
            loss_file.write("test image:")
            loss_file.write(str(test_image))
            loss_file.write(str(loss) + '\n')
            loss_file.close()
   
        
        #Save and print the output layer:
        output_arr = make_matrix(net, out_name)
        if print_output_per_iter:
            for image in range(no_outputs):
                fig = plt.figure()
                plt.imshow(output_arr[image], interpolation='none')
            

            
        if SAVE:
            #Save images in results folder:
            for output_no in range(no_outputs):
                if scale_op:
                    plt.imsave(test_dir + '/OUTPUT' + str(output_no) + '_ITER_' + str(iteration) + '-' + str(test_image) + '.png', output_arr[output_no])
                else:
                    plt.imsave(test_dir + '/OUTPUT' + str(output_no) + '_ITER_' + str(iteration) + '-' + str(test_image) + '.png', output_arr[output_no], vmin=0, vmax=1)
            
###############################################################################   
if SAVE:
    #Summary file:
    summary = open(test_dir + '/MPsummary.txt', 'w')
    summary.write('''
    ADDITIONAL INFORMATION:
    Now prevents dynamic range scaling :)
    ''')
    summary.write('\nKernel Sizes ' + str(K_size) )
    summary.close()

###############################################################################
#DISPLAY THE BLOBS:
if print_blobs:
    layer_arrs = []
    for blob in net.blobs.keys()[5:-2]:
        curr_blob = make_matrix(net, blob)
        layer_arrs.append(curr_blob)
        for blob_layer in range(len(curr_blob)):
            fig = plt.figure()
            plt.imshow(curr_blob[blob_layer][:][:], interpolation='none')
            fig.suptitle('BLOB_' + blob + str(blob_layer))
            



##DISPLAY THE WEIGHTS:
if print_weights: 
    params = net.params.keys()[0:]
    weights = []
    for net_layer in params:
        weight_layer = net.params[net_layer][0].data
        weights.append(weight_layer)
        for weight_slice in range(len(weight_layer)):
            fig = plt.figure()
            min_w = np.min(weight_layer[weight_slice][0])
            max_w = np.max(weight_layer[weight_slice][0])
            plt.imshow(weight_layer[weight_slice][0], interpolation='none', vmax=max_w, vmin=min_w)
            fig.suptitle('WEIGHT_' + net_layer + '_' + str(weight_slice))
            
            if save_weights:
                ##Save the weights
                try:
                    os.mkdir(test_dir + '/weights')
                except:
                    pass
                #Upscale:
                weight_ds = weight_layer[weight_slice][0]
                weight_us = np.zeros((len(weight_ds) * 100, len(weight_ds) * 100))
                for xx in range(len(weight_ds) * 100):
                    for yy in range(len(weight_ds) * 100):
                        weight_us[xx, yy] = weight_ds[math.floor(xx/100)][math.floor(yy/100)]
                plt.imsave(test_dir + '/weights/' + 'WEIGHT_' + net_layer + '_' + str(weight_slice) + '.png', weight_us, vmax=max_w, vmin=min_w)


















