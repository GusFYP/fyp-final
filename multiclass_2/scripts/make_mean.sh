#!/usr/bin/env sh
# Compute the mean image from the imagenet training lmdb
# N.B. this is available in data/ilsvrc12

#Input Data
EXAMPLE=data/train/lmdb/
#Output
DATA=data/mean
#Location of caffe functions
TOOLS=/home/gus/caffe/cmake_build/tools

$TOOLS/compute_image_mean $EXAMPLE/imgs \
  $DATA/mean.binaryproto

echo "Done."
