# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 13:53:45 2015

@author: gus
"""
import os

def create_directory_tree (base_dir, dir_type):
    os.mkdir(base_dir + '/data/' + dir_type)
    os.mkdir(base_dir + '/data/' + dir_type + '/raw')
    os.mkdir(base_dir + '/data/' + dir_type + '/raw' + '/imgs')
    os.mkdir(base_dir + '/data/' + dir_type + '/raw' + '/lbls')
    os.mkdir(base_dir + '/data/' + dir_type + '/lmdb')
    