# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 14:18:47 2015

@author: gus
"""
import os
import shutil
from create_directory_tree import create_directory_tree

def make_folder_structure(base_dir):
    #Clear old data and recreate folder structure:
    #Delete all old data:
    if os.path.exists(base_dir + '/data'):
        shutil.rmtree(base_dir + '/data')
    if os.path.exists(base_dir + '/snapshots'):
        shutil.rmtree(base_dir + '/snapshots')
    #create snapshots folder:
    os.mkdir(base_dir + '/snapshots')
    #Recreate Image and LMDB folder structure:
    os.mkdir(base_dir + '/data')
    os.mkdir(base_dir + '/data/mean')
    create_directory_tree(base_dir, 'train')
    create_directory_tree(base_dir, 'test')
    create_directory_tree(base_dir, 'val')