# -*- coding: utf-8 -*-
"""
Created on Tue Aug 11 18:32:18 2015

@author: gus
"""
import numpy as np
import random
from scipy.misc import imsave

#Metaparameters:
cross_length = 10
circle_rad = 5

#Creates a specified number of the shape images
#Also creates a text file with the list of all the images:
def make_shapes_new(data_type, no_iter, no_shapes, data_dir):
    #Create image and label lists
    data_im = open(data_dir + data_type +'/raw/listim.txt', 'w') 
    data_lb = open(data_dir + data_type +'/raw/listlb.txt', 'w') 
    
    #Make input images and labels
    for iter_number in range(no_iter):
        ##############################
        #Starting matrices
        matrix_im = np.zeros((512, 512, 3))
        matrix_lb = np.zeros((512, 512, 3))
        matrix_lb[:, :, 2] = np.ones((512, 512))
        
        for x in range(no_shapes):
            ##############################
            #cross
            #Generate a random centre:
            centre_x = random.randrange( 0 + cross_length , (512 - (cross_length)) )
            centre_y = random.randrange( 0 + cross_length , (512 - (cross_length)) )
            #Draw cross on image:
            #Horizontal bar
            matrix_im[centre_y - int(cross_length /2) : centre_y + int(cross_length /2),
                      centre_x,
                      :] = 1
            #Vertical bar
            matrix_im[centre_y,
                      centre_x - int(cross_length /2) : centre_x + int(cross_length /2),
                      :] = 1
            
            #Draw cross on label:
            #Horizontal bar
            matrix_lb[centre_y - int(cross_length /2) : centre_y + int(cross_length /2),
                      centre_x,
                      0] = 1
            #Vertical bar
            matrix_lb[centre_y,
                      centre_x - int(cross_length /2) : centre_x + int(cross_length /2),
                      0] = 1
                      
            #create the "nothing" label:
            #Horizontal bar
            matrix_lb[centre_y - int(cross_length /2) : centre_y + int(cross_length /2),
                      centre_x,
                      2] = 0
            #Vertical bar
            matrix_lb[centre_y,
                      centre_x - int(cross_length /2) : centre_x + int(cross_length /2),
                      2] = 0
            
            ##############################
            #Circle
            #Generate a random circle:
            centre_x = random.randrange( 0 + circle_rad, (512 - (circle_rad + 1)) )
            centre_y = random.randrange( 0 + circle_rad, (512 - (circle_rad + 1)) )
            for xx in range(0, 512):
                for yy in range(0, 512):
                    circle = ((xx - centre_x)**2 + (yy - centre_y)**2)
                    #If the pixel falls within the circle
                    if circle < circle_rad**2:
                        matrix_im[yy, xx, :] = 1
                        matrix_lb[yy, xx, 1] = 1
                        #Create the "nothing" label
                        matrix_lb[yy, xx, 2] = 0
    
            ##############################
    
        #Save the image and label and add the image to the list of images:
        #Image:
        imsave(data_dir + data_type + '/raw/imgs/image_' + str(iter_number) + '.png', matrix_im)
        data_im.write('imgs/image_' + str(iter_number) + '.png 1\n')    
        #Label:
        imsave(data_dir + data_type + '/raw/lbls/label_' + str(iter_number) + '.png', matrix_lb)
        data_lb.write('lbls/label_' + str(iter_number) + '.png 1\n') 
                    
                    
    return matrix_im, matrix_lb

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        

    
    
    