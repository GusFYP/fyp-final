# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 13:37:16 2015

@author: gus
"""

#Import Modules:
from make_folder_structure import make_folder_structure
from make_shapes_5_class import make_shapes_5_class
from make_shapes_2_class import make_shapes_2_class
from make_shapes_3_class import make_shapes_3_class
from make_shapes_4_class import make_shapes_4_class

#Specify metaparameters
no_iter_train = 200
no_iter_test = 20
no_iter_val = 20
no_shapes = 15
base_dir = '/home/gus/repos/multiclass_2'
data_dir = base_dir + '/data/'

###############################################################################
#Set up folder structure:
make_folder_structure(base_dir)

#Create traing data
make_shapes_4_class('train', no_iter_train, no_shapes, data_dir)

#Create test data
make_shapes_4_class('test', no_iter_test, no_shapes, data_dir)

#Create validation data
make_shapes_4_class('val', no_iter_val, no_shapes, data_dir)














