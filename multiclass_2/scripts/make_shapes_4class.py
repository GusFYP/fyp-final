# -*- coding: utf-8 -*-
"""
Created on Tue Aug 11 18:32:18 2015

@author: gus
"""
import numpy as np
import random
from scipy.misc import imsave

#Metaparameters:
square_len = 5
triangle_height = 5
circle_rad = 2

#Creates a specified number of the shape images
#Also creates a text file with the list of all the images:
def make_shapes(data_type, no_iter, no_shapes, data_dir):
    #Create image and label lists
    data_im = open(data_dir + data_type +'/raw/listim.txt', 'w') 
    data_lb = open(data_dir + data_type +'/raw/listlb.txt', 'w') 
    
    #Make input images and labels
    for iter_number in range(no_iter):
        ##############################
        #Starting matrices
        matrix_im = np.zeros((512, 512, 4))
        matrix_lb = np.zeros((512, 512, 4))
        matrix_lb[:, :, 3] = np.ones((512, 512))
        
        for x in range(no_shapes):
            ##############################
            #Squares:
            #Generate a random top left corner:
            TL_corner_x = random.randrange( 0 , (512 - (square_len + 1)) )
            TL_corner_y = random.randrange( 0 , (512 - (square_len + 1)) )
            #Draw square on image:
            matrix_im[TL_corner_y : TL_corner_y + square_len,
                      TL_corner_x : TL_corner_x + square_len,
                      :] = 1
            #Draw square on label:
            matrix_lb[TL_corner_y : TL_corner_y + square_len,
                      TL_corner_x : TL_corner_x + square_len,
                      0] = 1
            #create the "nothing" label:
            matrix_lb[TL_corner_y : TL_corner_y + square_len,
                      TL_corner_x : TL_corner_x + square_len,
                      3] = 0
            
            ##############################
            #Circles:
            #Generate a random cirlce centre:
            centre_x = random.randrange( 0 + circle_rad, (512 - (circle_rad + 1)) )
            centre_y = random.randrange( 0 + circle_rad, (512 - (circle_rad + 1)) )
            for xx in range(0, 512):
                for yy in range(0, 512):
                    circle = ((xx - centre_x)**2 + (yy - centre_y)**2)
                    #If the pixel falls within the circle
                    if circle < circle_rad**2:
                        matrix_im[yy, xx, :] = 1
                        matrix_lb[yy, xx, 1] = 1
                        #Create the "nothing" label
                        matrix_lb[yy, xx, 3] = 0
    
            ##############################
            #Triangles:
            #Randomly choose between upward pointing or downward pointing:
            direction = random.choice((-1, 1))
            #Generate a random triangle apex:
            apex_x = random.randrange(0 + triangle_height, 511 - triangle_height )
            apex_y = random.randrange( 0 + triangle_height, 511 - triangle_height )
    
            for layer in  range(triangle_height):
                #Draw triangle on image
                matrix_im[apex_y + layer * direction,
                          apex_x - layer : apex_x + layer,
                          :] = 1
                #Draw triangle on label
                matrix_lb[apex_y + layer * direction,
                          apex_x - layer : apex_x + layer,
                          2] = 1
                #Create the 'nothing' label
                matrix_lb[apex_y + layer * direction,
                          apex_x - layer : apex_x + layer,
                          2] = 0
                          
        ##############################
        #Save the image and label and add the image to the list of images:
        #Image:
        imsave(data_dir + data_type + '/raw/imgs/image_' + str(iter_number) + '.png', matrix_im)
        data_im.write('imgs/image_' + str(iter_number) + '.png 1\n')    
        #Label:
        imsave(data_dir + data_type + '/raw/lbls/label_' + str(iter_number) + '.png', matrix_lb)
        data_lb.write('lbls/label_' + str(iter_number) + '.png 1\n') 
                    
                    
    return matrix_im, matrix_lb

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        

    
    
    