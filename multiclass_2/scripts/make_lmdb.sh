#!/usr/bin/env sh
# Create the imagenet lmdb inputs
# N.B. set the path to the imagenet train + val data dirs

#Outputs (?)
EXAMPLE=data

DATA=data
#Location of Caffe functions
TOOLS=/home/gus/caffe/cmake_build/tools

#Source Data (?)
DATA_ROOT_TRAIN=/home/gus/repos/multiclass_2/data/train/raw/
DATA_ROOT_TEST=/home/gus/repos/multiclass_2/data/test/raw/
DATA_ROOT_VAL=/home/gus/repos/multiclass_2/data/val/raw/

# Set RESIZE=true to resize the images to 256x256. Leave as false if images have
# already been resized using another tool.
RESIZE=false
if $RESIZE; then
  RESIZE_HEIGHT=256
  RESIZE_WIDTH=256
else
  RESIZE_HEIGHT=0
  RESIZE_WIDTH=0
fi

echo "Creating train image lmdb..."

GLOG_logtostderr=1 $TOOLS/convert_imageset \
    --resize_height=$RESIZE_HEIGHT \
    --resize_width=$RESIZE_WIDTH \
    --gray \
    $DATA_ROOT_TRAIN \
    $DATA/train/raw/listim.txt \
    $EXAMPLE/train/lmdb/imgs

echo "Creating train label lmdb..."

GLOG_logtostderr=1 $TOOLS/convert_imageset \
    --resize_height=$RESIZE_HEIGHT \
    --resize_width=$RESIZE_WIDTH \
    --gray \
    $DATA_ROOT_TRAIN \
    $DATA/train/raw/listlb.txt \
    $EXAMPLE/train/lmdb/lbls

echo "Creating val image lmdb..."

GLOG_logtostderr=1 $TOOLS/convert_imageset \
    --resize_height=$RESIZE_HEIGHT \
    --resize_width=$RESIZE_WIDTH \
    --gray \
    $DATA_ROOT_VAL \
    $DATA/val/raw/listim.txt \
    $EXAMPLE/val/lmdb/imgs

echo "Creating val label lmdb..."

GLOG_logtostderr=1 $TOOLS/convert_imageset \
    --resize_height=$RESIZE_HEIGHT \
    --resize_width=$RESIZE_WIDTH \
    --gray \
    $DATA_ROOT_VAL \
    $DATA/val/raw/listlb.txt \
    $EXAMPLE/val/lmdb/lbls

echo "Creating test image lmdb..."

GLOG_logtostderr=1 $TOOLS/convert_imageset \
    --resize_height=$RESIZE_HEIGHT \
    --resize_width=$RESIZE_WIDTH \
    --gray \
    $DATA_ROOT_TEST \
    $DATA/test/raw/listim.txt \
    $EXAMPLE/test/lmdb/imgs

echo "Creating test label lmdb..."

GLOG_logtostderr=1 $TOOLS/convert_imageset \
    --resize_height=$RESIZE_HEIGHT \
    --resize_width=$RESIZE_WIDTH \
    --gray \
    $DATA_ROOT_TEST \
    $DATA/test/raw/listlb.txt \
    $EXAMPLE/test/lmdb/lbls

echo "Done."
