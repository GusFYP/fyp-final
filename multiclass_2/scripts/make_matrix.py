# -*- coding: utf-8 -*-
"""
Created on Fri Aug 14 15:40:25 2015

@author: gus
"""
import numpy as np

def make_matrix(network, layer_name):
    LAYER = network.blobs[layer_name].data
    shape_L = np.shape(LAYER)
    arr = []
    for layer in range(shape_L[-3]):
        arr.append(np.asarray([LAYER])[0, 0, layer, :, :])
        
    return arr