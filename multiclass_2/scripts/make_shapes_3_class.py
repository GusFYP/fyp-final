# -*- coding: utf-8 -*-
"""
Created on Tue Aug 11 18:32:18 2015

@author: gus
"""
import numpy as np
import random
from scipy.misc import toimage
import matplotlib.pyplot as plt

#Metaparameters:
cross_length = 6
circle_rad = 3
square_length = 4
box_length = 5
triangle_height = 5

#Creates a specified number of the shape images
#Also creates a text file with the list of all the images:
def make_shapes_3_class(data_type, no_iter, no_shapes, data_dir):
    #Create image and label lists
    data_im = open(data_dir + data_type +'/raw/listim.txt', 'w') 
    data_lb = open(data_dir + data_type +'/raw/listlb.txt', 'w') 
    
    #Make input images and labels
    for iter_number in range(no_iter):
        
        ##############################
        #Starting matrices
        matrix_im = np.zeros((512,512,3))
        matrix_lb = np.zeros((512,512,3))
        
        for x in range(no_shapes):
            
            ##############################
            #Hollow box:
            #Generate a random hollow box:
            corner_tl_x = random.randrange( 0 + square_length, (512 - (square_length + 1)) )
            corner_tl_y = random.randrange( 0 + square_length, (512 - (square_length + 1)) )
            #draw box:
            matrix_im[corner_tl_y: corner_tl_y + square_length,
                      corner_tl_x: corner_tl_x + square_length,
                      :] = 1
            #Subtract hollow part:
            matrix_im[corner_tl_y + 1: corner_tl_y + square_length - 1,
                      corner_tl_x + 1: corner_tl_x + square_length - 1,
                      :] = 0                     
            #label:
            #draw box:
            matrix_lb[corner_tl_y: corner_tl_y + square_length,
                      corner_tl_x: corner_tl_x + square_length,
                      :] = 3
            #Subtract hollow part:
            matrix_lb[corner_tl_y + 1: corner_tl_y + square_length - 1,
                      corner_tl_x + 1: corner_tl_x + square_length - 1,
                      :] = 0    
            
            ##############################
            #cross
            #Generate a random centre:
            centre_x = random.randrange( 0 + cross_length , (512 - (cross_length)) )
            centre_y = random.randrange( 0 + cross_length , (512 - (cross_length)) )
            #Draw cross on image:
            #Horizontal bar
            matrix_im[centre_y - int(cross_length /2) : centre_y + int(cross_length /2),
                      centre_x,
                      :] = 1
            
            #Vertical bar
            matrix_im[centre_y,
                      centre_x - int(cross_length /2) : centre_x + int(cross_length /2),
                      :] = 1
                      
            #Draw cross on label:
            #Horizontal bar
            matrix_lb[centre_y - int(cross_length /2) : centre_y + int(cross_length /2),
                      centre_x,
                      :] = 1
            
            #Vertical bar
            matrix_lb[centre_y,
                      centre_x - int(cross_length /2) : centre_x + int(cross_length /2),
                      :] = 1
                      
            
            ##############################
            #Circle
            #Generate a random circle:
            centre_x = random.randrange( 0 + circle_rad, (512 - (circle_rad + 1)) )
            centre_y = random.randrange( 0 + circle_rad, (512 - (circle_rad + 1)) )
            for xx in range(0, 512):
                for yy in range(0, 512):
                    circle = ((xx - centre_x)**2 + (yy - centre_y)**2)
                    #If the pixel falls within the circle
                    if circle <= circle_rad**2:
                        matrix_im[yy, xx, :] = 1
                        matrix_lb[yy, xx, :] = 2
                        
      
        
    
        #Save the image and label and add the image to the list of images:
        #Image:
        plt.imsave(data_dir + data_type + '/raw/imgs/image_' + str(iter_number) + '.png', matrix_im)
        data_im.write('imgs/image_' + str(iter_number) + '.png 1\n')    
        #Label:
        data_lb.write('lbls/label_' + str(iter_number) + '.png 1\n') 
        #scipy used because it doesn't rescale the data: http://stefaanlippens.net/scipy_unscaledimsave
        toimage(matrix_lb, cmin=0, cmax=255).save(data_dir + data_type + '/raw/lbls/label_' + str(iter_number) + '.png')    
                    
    return matrix_im, matrix_lb

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        

    
    
    