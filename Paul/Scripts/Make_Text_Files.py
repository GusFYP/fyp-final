# -*- coding: utf-8 -*-
"""
Created on Wed Sep  9 18:36:34 2015

@author: gus
"""

import os

Data_Dir = '/home/gus/repos/Paul/Data/'



def make_TF(data_type):
    
    root, lbls, imgs = os.walk(Data_Dir + data_type + '/raw')
    print("labels:", lbls)
    print("Images:", imgs)
    for each_dir in root[1]:
        text_list = open(Data_Dir + data_type + '/raw/list_' + each_dir + '.txt', 'w')
        if each_dir == 'lbls':
            for each_label in sorted(lbls[2]):
                if each_label[-1] == 'G':
                    text_list.write(each_dir + '/' + each_label + ' 1\n')
        elif each_dir == 'imgs':
            for each_image in sorted(imgs[2]):
                if each_image[-1] == 'g':
                    text_list.write(each_dir + '/' + each_image + ' 1\n')
        else:
            print("error")


            
        
make_TF('test')
make_TF('train')
make_TF('val')
        