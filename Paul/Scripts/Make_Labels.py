# -*- coding: utf-8 -*-
"""
Created on Sun Sep  6 17:59:53 2015

@author: gus
"""
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.misc import toimage

Paul_Label_Dir = "/home/gus/repos/Paul/Data/Small_Paul_Labels/"
Paul_Cache_Dir = "/home/gus/repos/Paul/Data/Small_Paul_CACHE/"

Gen_obj = os.walk(Paul_Cache_Dir)



for Paul_Images in Gen_obj:
    for Each_Paul in Paul_Images[2]:
        #Open labelled images:
        Paul_Opened = plt.imread(Paul_Cache_Dir + Each_Paul)
        
        #Take only the alpha layer:
        Eye_Label = np.ceil(Paul_Opened[:, :, 3])
        #Invert so that we are labelling the eyes themselves (eyes = 1):
        Paul_Label = 1 - Eye_Label
        #Round the boundary values UP to one:
        Paul_Label = np.ceil(Paul_Label)
        
        
        #make the NN labels:
        toimage(Paul_Label, cmin=0, cmax=255).save(Paul_Label_Dir + Each_Paul)
        
        
        
        
        

