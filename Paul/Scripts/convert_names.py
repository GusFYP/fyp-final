# -*- coding: utf-8 -*-
"""
Created on Tue Sep 29 12:43:35 2015

@author: gus
"""

#Convert Image Names
import os
import numpy as np

im_direct = []
im_direct.append('/home/gus/Desktop/RESULTS 2/TEST_14/')
im_direct.append('/home/gus/Desktop/RESULTS 2/TEST_15/')
im_direct.append('/home/gus/Desktop/RESULTS 2/TEST_16/')
im_direct.append('/home/gus/Desktop/RESULTS 2/TEST_17/')

#For all the listed directories
for image_types in im_direct:
    #For all the test images
    for image_num in range(8):
        #Get a list of all image names
        all_images = os.listdir(image_types + "im%d/" % image_num)
        #Check the number of total images
        num_images = len(all_images)
        #Work out how many digits in total (for appending leading zeros)
        digit_number = np.floor(np.log10(num_images)) + 1
        
        #For each individual image:
        for im_num, each_image in enumerate(all_images):
            #append zeros equal to the number of total digits needed minus the digits needed for thsi number
            new_name = '0' * (digit_number - (np.floor(np.log10(im_num + 1)) + 1)) + str(im_num + 1)
            os.rename(image_types + "/im%d/" % image_num + each_image, image_types + "/im%d/" % image_num + new_name + '.png')
    