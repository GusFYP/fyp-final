# -*- coding: utf-8 -*-
"""
Created on Wed Sep  9 14:10:57 2015

@author: gus
"""

#Convert to PNG:

import os
import matplotlib.pyplot as plt

Paul_Dir = "/home/gus/repos/Paul/Data/Small_Paul/"
Paul_Cache_Dir = "/home/gus/repos/Paul/Data/Small_Paul_CACHE/"

Gen_obj = os.walk(Paul_Dir)

for Paul_Images in Gen_obj:
    for Each_Paul in Paul_Images[2]:
        Paul_Opened = plt.imread(Paul_Dir + Each_Paul)
        
        #SAVE AS PNG:
        png_string = Paul_Cache_Dir + str(Each_Paul)[:-3] + 'PNG'
        plt.imsave(png_string,  Paul_Opened)
        print('hey!')