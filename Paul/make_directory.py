# -*- coding: utf-8 -*-
"""
Created on Sat Aug  1 13:10:08 2015

@author: gus
"""
import os

def make_directory(base_dir, directory):
    #Split the directory into the folder to create:
    dir_list = directory.split( "/" )
    
    #Create each folder one by one:
    for folder in range(0, len(dir_list)):
        base_dir = base_dir + '/' + dir_list[folder]
        try:
            os.mkdir(base_dir)
        except:
            pass
        
    #See if there are already any folders here:
    for test_number in range(0, 10000):
        try:
            os.mkdir(base_dir + '/TEST_' + str(test_number))
            curr_dir = base_dir + '/TEST_' + str(test_number)
            break
        except:
            continue
        
    return curr_dir
        

