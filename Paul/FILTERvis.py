# -*- coding: utf-8 -*-
"""
Created on Sat Oct  3 21:22:35 2015

@author: gus
"""

#Filter Visualisation

#Setting up and Module Imports
import sys
caffe_root = '/home/gus/caffe/' 
sys.path.insert(0, caffe_root + 'python')
import caffe
import matplotlib.pyplot as plt

from matplotlib import pylab

import numpy as np
import scipy as sp

sys.path.insert(0, '/home/gus/repos/Paul/scripts')
from make_matrix import make_matrix

###############################################################################
#SPECIFIY DISPLAY METAPARAMETERS HERE:
iteration = 60000
num_tests = 1
SAVE = 1
separate_folders = 0
##
print_blobs = 0
##
print_output_per_iter = 0

##
print_weights = 1
##
scale_op = 0

folder_path = "/home/gus/repos/Paul/smart_results/NN_STRUCT_['20', '30', '40', '40', '40', '50', '2']/LNR_RATE_0.000000015/Paul softmax/SMPoolLossLayer/TEST_15"
solver_name = '/SOLVER.prototxt'
layer_name = '/LAYERS.prototxt'


    
###############################################################################
#Specify trained model and NN structure:
MODEL_FILE = folder_path + layer_name
PRETRAINED = folder_path + '/_iter_' + str(iteration) + '.caffemodel'
net = caffe.Net(MODEL_FILE, PRETRAINED,caffe.TEST)

def get_filters(net):
                
    #Extract the weights:
    params = net.params.keys()
    weights = []
    for net_layer in params:
        weight_layer = net.params[net_layer][0].data
        weights.append(weight_layer)
        
    ###############################################################################
    #GETTING THE FILTERS:
        
    #first, get the filter sizes:
    filter_sizes = []
    
    for kernel in weights:
        if not len(filter_sizes):
            filter_sizes.append(np.shape(kernel)[3])
        else:
            filter_sizes.append(filter_sizes[-1] + np.floor(np.shape(kernel)[3] / 2))
            
    #now, find the maximum response for each layer and get the part of the input image which corresponds:
    max_current = np.zeros((7, 50))
    
    #Create the image cache:
    image_cache = []
    for x in range(7):
        image_cache.append(np.zeros( (np.shape(weights[x])[0],3, filter_sizes[x], filter_sizes[x]) ))
    
            
    for image_num in range(num_tests):
        #forward propagate:
        net.forward()
        
        #Extract the blobs:
        layer_arrs = []
        
        print(net.blobs.keys())
        for blob in net.blobs.keys()[6:-3:2]:
            curr_blob = make_matrix(net, blob)
            layer_arrs.append(np.array(curr_blob))
            
        #Get the maximum of each layer and store the value and index
        for filter_idx, filter_layer in enumerate(layer_arrs):
            for filter_slice in range(np.shape(filter_layer)[0]):
                max_value = np.max(filter_layer[filter_slice])
                max_index = np.where(filter_layer[filter_slice] == max_value)
                max_index = (max_index[0][0], max_index[1][0])
                
                #Check if it's the current max, and if so, replace the current max:
                if max_value > max_current[filter_idx, filter_slice] and 239 > max_index[0] > 11 and 239 > max_index[1] > 11:
                    max_current[filter_idx, filter_slice] = max_value
                    
                    #Create the filters:
                    offset = filter_sizes[filter_idx]
                    index_start = [max_index[0] - np.floor(offset / 2), max_index[1] - np.floor(offset / 2)]
                    cache = np.array(make_matrix(net, 'data'))
                    cache_2 = cache[:, int(index_start[0]):int(index_start[0]) + offset, int(index_start[1]):int(index_start[1]) + offset]
                
                    image_cache[filter_idx][filter_slice][:][:][:] = cache_2
                    
    return image_cache
                    

            
#Display filters:
def show_filters(filters, level, save):
    #Create Figure:
    fig = plt.figure()
        
    for im_num, images in enumerate(filters[level]):
        im_shape = np.shape(filters[level])
        if im_shape[0] >= 10:    
            rows = np.ceil(im_shape[0] / 5)
            cols = 5
        else:
            rows = 1
            cols = im_shape[0]
        
        #Change how the images are stacked:
        shape_im = np.shape(images)
        images_rgb = np.zeros((shape_im[1], shape_im[2], shape_im[0]))
        for channel in range(shape_im[0]):
            images_rgb[:, :, channel] = images[channel, :, :]
            
            #increase size of image:
            images_rgb_big = sp.misc.imresize(images_rgb, 1000)
            
        #Create subplots:
        ax = fig.add_subplot(rows, cols, im_num)
        #Add image to the figure:
        ax.imshow(images_rgb_big)
        ax.axes.get_xaxis().set_visible(False)
        ax.axes.get_yaxis().set_visible(False)
        
    #Save image:
    if save:
        try:
            os.mkdir('/home/gus/Desktop/RESULTS 2/Weights/level%d' % level)
        except:
            pass
        
        fig.savefig('/home/gus/Desktop/RESULTS 2/Weights/level%d/filters.png' % level)
        
#Display filters:
def show_sample_filter(sample_filter, save):
    #Create Figure:
    fig = plt.figure()
        
    for im_num, images in enumerate(sample_filter):

        #Change how the images are stacked:
        shape_im = np.shape(images)
        images_rgb = np.zeros((shape_im[1], shape_im[2], shape_im[0]))
        for channel in range(shape_im[0]):
            images_rgb[:, :, channel] = images[channel, :, :]
            
            #increase size of image:
            images_rgb_big = sp.misc.imresize(images_rgb, 1000)
            
        #Create subplots:
        ax = fig.add_subplot(2, 5, im_num)
        #Add image to the figure:
        ax.imshow(images_rgb_big)
        ax.axes.get_xaxis().set_visible(False)
        ax.axes.get_yaxis().set_visible(False)
        
    #Save image:
    if save:
        try:
            os.mkdir('/home/gus/Desktop/RESULTS 2/Weights/sample')
        except:
            pass
        
        fig.savefig('/home/gus/Desktop/RESULTS 2/Weights/sample/filters.png')


                    
def get_sample_filter(net, layer, slice_num):
                
    #Extract the weights:
    params = net.params.keys()
    weights = []
    for net_layer in params:
        weight_layer = net.params[net_layer][0].data
        weights.append(weight_layer)
        
    ###############################################################################
    #GETTING THE FILTERS:
        
    #first, get the filter sizes:
    filter_sizes = []
    
    for kernel in weights:
        if not len(filter_sizes):
            filter_sizes.append(np.shape(kernel)[3])
        else:
            filter_sizes.append(filter_sizes[-1] + np.floor(np.shape(kernel)[3] / 2))
            
    #now, find the 10 highest responses for our chosen layer, and get the part of the input image which corresponds:
    max_current = [0]*10
    
    #Create the image cache:
    image_cache = []
    for x in range(10):
        image_cache.append(np.zeros((3, filter_sizes[layer], filter_sizes[layer]) ))
    
            
    for image_num in range(num_tests):
        #forward propagate:
        net.forward()
        
        #Extract the blobs:
        layer_arrs = []
        
        for blob in net.blobs.keys()[6:-3:2]:
            curr_blob = make_matrix(net, blob)
            layer_arrs.append(np.array(curr_blob))
            
        filter_layer = layer_arrs[layer]
        
        max_value = np.max(filter_layer[slice_num])
        max_index = np.where(filter_layer[slice_num] == max_value)
        max_index = (max_index[0][0], max_index[1][0])
        
        #Check if it's big enough to fit in the current max array:
        if max_value < max_current[9] and 239 > max_index[0] > 11 and 239 > max_index[1] > 11:
            pass
        else:
            for elem in range(10):
                if max_value > max_current[elem] and 239 > max_index[0] > 11 and 239 > max_index[1] > 11:
                    max_current.pop()
                    max_current.insert(elem, max_value)
                    
                    #Create the filters:
                    offset = filter_sizes[layer]
                    index_start = [max_index[0] - np.floor(offset / 2), max_index[1] - np.floor(offset / 2)]
                    cache = np.array(make_matrix(net, 'data'))
                    cache_2 = cache[:, int(index_start[0]):int(index_start[0]) + offset, int(index_start[1]):int(index_start[1]) + offset]
                
                    image_cache.pop()
                    image_cache.insert(elem, cache_2)
                    
                    #break out of loop
                    break
                    
    return image_cache, max_current
    
def save_weight(layer_num):
    fig = plt.figure()
    #Extract the weights:
    params = net.params.keys()
    weights = []
    for net_layer in params:
        weight_layer = net.params[net_layer][0].data
        weights.append(weight_layer)
        
    for im_num, weight in enumerate(weights[layer_num]):
        w_shape = np.shape(weight)
        weightRGB = np.zeros((w_shape[1], w_shape[2], w_shape[0]))
        for x in range(3):
            weightRGB[:, :, x] = weight[x, :, :]
            
        #Create subplots:
        ax = fig.add_subplot(4, 5, im_num)
        #Add image to the figure:
        ax.imshow(weightRGB, interpolation='None')
        ax.axes.get_xaxis().set_visible(False)
        ax.axes.get_yaxis().set_visible(False)
        
        #Save image:
        if save:
            try:
                os.mkdir('/home/gus/Desktop/RESULTS 2/Weights/sample_filter')
            except:
                pass
        
        fig.savefig('/home/gus/Desktop/RESULTS 2/Weights/sample/ll_filters.png')

        
    
    
            
#filters = get_filters(net)
            
##SAVE FILTERS:
#for f_num in range(7):
#    show_filters(filters, f_num, 1)
            
            
#sample_filter = get_sample_filter(net, 4, 0)
#
##SAVE FILTERS:
#show_sample_filter(sample_filter[0], 1)
#
#save_weight(0)

































